package org.openhab.habdroid.ui;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import org.openhab.habdroid.R;
import org.openhab.habdroid.core.connection.Connection;
import org.openhab.habdroid.core.connection.ConnectionFactory;
import org.openhab.habdroid.core.connection.exception.ConnectionException;
import org.openhab.habdroid.model.ChangedItem;
import org.openhab.habdroid.model.LinkedPage;
import org.openhab.habdroid.model.Recognition;
import org.openhab.habdroid.model.Widget;
import org.openhab.habdroid.util.Util;
import org.openhab.habdroid.util.GetDataService;
import org.openhab.habdroid.util.RetrofitClientInstance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.widget.TextView;
import android.widget.Toast;

/*
 * Activity for OpenLicht Recognition
 * By Muhammad Ibrahim Rahman
 */

public class RecognitionsActivity extends AppCompatActivity implements WidgetAdapter.ItemClickListener {

    private RecognitionsAdapter mPastRecognitionsAdapter;
    private RecyclerView mRvPastRecognitions;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Util.setActivityTheme(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recognitions);

        Toolbar toolbar = findViewById(R.id.openhab_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefresh_recognitions);
        mSwipeRefreshLayout.setOnRefreshListener(() -> getRESTData());

        if (savedInstanceState == null) {
            getRESTData();
        }

        updateTitle();
        setResult(RESULT_OK);
    }

    private void getRESTData() {
        progressDialog = new ProgressDialog(RecognitionsActivity.this);
        progressDialog.setMessage("Loading");
        if (!mSwipeRefreshLayout.isRefreshing()) progressDialog.show();

        //  Interface for the RetrofitInstance
        try {
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(this).create(GetDataService.class);
            Call<List<Recognition>> call = service.getAllRecognitions();
            call.enqueue(new Callback<List<Recognition>>() {

                @Override
                public void onResponse(Call<List<Recognition>> call, Response<List<Recognition>> response) {

                    progressDialog.dismiss();
                    //generateDataList(response.body());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        response.body().sort((o1, o2) -> o2.getTimestamp() - o1.getTimestamp());
                    }
                    generateCurrentActivity(response.body().get(0));
                    generatePastActivities(response.body().subList(1, response.body().size()));
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(Call<List<Recognition>> call, Throwable t) {
                    progressDialog.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(RecognitionsActivity.this, "Error loading Recognition. Please check your network connection.", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (IllegalArgumentException e) {
            progressDialog.dismiss();
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }


    }

    private void generateCurrentActivity(Recognition recognition) {
        TextView mCurrentActivityTitle = findViewById(R.id.text_recognition_current_title);
        TextView mCurrentActivityTime = findViewById(R.id.text_recognition_current_time);
        RecyclerView mCurrentActivityChangedItems = findViewById(R.id.rv_recognition_current_items);
        mCurrentActivityTitle.setText(recognition.getDescription().toString());
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.US);
        mCurrentActivityTime.setText(dateFormat.format(new Date(((long) recognition.getTimestamp()) * 1000)));
        ArrayList<Widget> widgets = getIntent().getParcelableArrayListExtra("widgets");
        ArrayList<Widget> activityWidgets = new ArrayList<>();
        for (ChangedItem ci : recognition.getChangedItems()) {
            for (Widget widget : widgets) {
                if (widget.item() != null && ci.getName().equals(widget.item().name())) {
                    activityWidgets.add(widget);
                }
            }
        }

        try {
            Connection c = ConnectionFactory.getUsableConnection();
            if (c != null) {
                WidgetAdapter mChangedItemsAdapter = new WidgetAdapter(this, c, this);
                mChangedItemsAdapter.update(activityWidgets, false);
                mCurrentActivityChangedItems.setLayoutManager(new LinearLayoutManager(this));
                mCurrentActivityChangedItems.setAdapter(mChangedItemsAdapter);
            }
        } catch (ConnectionException e) {
            Toast.makeText(this, "No connection", Toast.LENGTH_LONG).show();
        }
    }

    private void generatePastActivities(List<Recognition> recognitions) {
        ArrayList<Recognition> filteredRecognitions = new ArrayList<>();
        for (Recognition r : recognitions) {
            if (r.getType().equals("recognition")) {
                filteredRecognitions.add(r);
            }
        }
        mRvPastRecognitions = findViewById(R.id.rv_past_recognitions);
        mPastRecognitionsAdapter = new RecognitionsAdapter(this, filteredRecognitions);
        mRvPastRecognitions.setLayoutManager(new LinearLayoutManager(this));
        mRvPastRecognitions.setAdapter(mPastRecognitionsAdapter);
    }

    /*
    //  Generates a List using RecyclerView with the RecognitionsAdapter
    private void generateDataList(List<Recognition> recognitionsList) {
        recyclerView = findViewById(R.id.customRecyclerView);
        adapter = new RecognitionsAdapter(this,recognitionsList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(RecognitionsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        //Util.overridePendingTransition(this, true);
    }

    private void updateTitle() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        @StringRes int titleResId = count > 0
                ? fm.getBackStackEntryAt(count - 1).getBreadCrumbTitleRes()
                : R.string.recognitions_title;
        setTitle(titleResId);
    }

    @Override
    public boolean onItemClicked(Widget item) {
        return false;
    }

    @Override
    public boolean onItemLongClicked(Widget item) {
        return false;
    }
}
