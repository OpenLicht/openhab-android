package org.openhab.habdroid.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.openhab.habdroid.R;
import org.openhab.habdroid.model.ChangedItem;
import org.openhab.habdroid.model.Recognition;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

/*
 *  OpenLicht
 *  Displaying to RecyclerView
 *  By Muhammad Ibrahim Rahman
 */

public class RecognitionsAdapter extends RecyclerView.Adapter<RecognitionsAdapter.CustomViewHolder> {

    private List<Recognition> mRecognitions;
    private Context mContext;

    public RecognitionsAdapter(Context context, List<Recognition> recognitions){
        this.mContext = context;
        this.mRecognitions = recognitions;

    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        final View mView;

        TextView recognitionTitle;
        TextView recognitionTime;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            recognitionTime = itemView.findViewById(R.id.text_recognition_past_time);
            recognitionTitle = itemView.findViewById(R.id.text_recognition_past_title);
        }
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_past_recognition, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Recognition item = mRecognitions.get(position);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.US);
        holder.recognitionTime.setText(dateFormat.format(new Date(((long)item.getTimestamp())*1000)));
        holder.recognitionTitle.setText(item.getDescription().toString());
    }

    @Override
    public int getItemCount() {
        return mRecognitions == null ? 0 : mRecognitions.size();
    }
}
