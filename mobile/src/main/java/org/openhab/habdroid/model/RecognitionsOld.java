package org.openhab.habdroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  OpenLicht
 *  Recognition data as POJO
 *  By Muhammad Ibrahim Rahman
 */

public class RecognitionsOld {

    @SerializedName("identifier")
    @Expose
    private Integer identifier;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("activity")
    @Expose
    private String activity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;

    public RecognitionsOld(Integer identifier, String type, String activity, String description, Integer timestamp) {
        this.identifier = identifier;
        this.type = type;
        this.activity = activity;
        this.description = description;
        this.timestamp = timestamp;
    }

    public Integer getIdentifier() {

        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
}
