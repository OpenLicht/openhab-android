package org.openhab.habdroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangedItem {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("state")
    @Expose
    private String state;

    private ChangedItemType type;

    public enum ChangedItemType {
        ColorLamp,
        DimmableLamp,
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setType(ChangedItemType type) {
        this.type = type;
    }

    public ChangedItemType getType() {
        if (type == null)
            return ChangedItemType.ColorLamp;
        return type;
    }
}
