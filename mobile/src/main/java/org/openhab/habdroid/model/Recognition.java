package org.openhab.habdroid.model;

import android.util.Log;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  OpenLicht
 *  Recognition data as POJO
 *  By Muhammad Ibrahim Rahman
 */

public class Recognition {

    @SerializedName("identifier")
    @Expose
    private Integer identifier;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("activity")
    @Expose
    private Object activity;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("changed_items")
    @Expose
    private List<ChangedItem> changedItems = null;

    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getActivity() {
        return activity;
    }

    public void setActivity(Object activity) {
        this.activity = activity;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public List<ChangedItem> getChangedItems() {
        return changedItems;
    }

    public void setChangedItems(List<ChangedItem> changedItems) {
        this.changedItems = changedItems;
    }
}
