package org.openhab.habdroid.util;

import org.openhab.habdroid.model.Recognition;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/*
 *  OpenLicht
 *  All Endpoints are defined here
 *  By Muhammad Ibrahim Rahman
 */

public interface GetDataService {

    @GET("/events/long")
    Call<List<Recognition>> getAllRecognitions();

    @PUT("/feedback/{identifier}/newDefault")
    Call<ResponseBody> setNewDefaultValue(@Path("identifier") String identifier);


//    @GET("/recognitions")
//    Call<List<Recognition>> getAllRecognitions();
}
