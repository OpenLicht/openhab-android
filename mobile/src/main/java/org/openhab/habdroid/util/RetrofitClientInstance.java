package org.openhab.habdroid.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/*
 *  OpenLicht
 *  Use Retrofit to call the API
 *  By Muhammad Ibrahim Rahman
 */

public class RetrofitClientInstance {

    private static Retrofit retrofit;


    public static Retrofit getRetrofitInstance(Context activity) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        String url = preferences.getString(Constants.PREFERENCE_OPENLICHT_URL, "");
        if (retrofit == null || !retrofit.baseUrl().toString().equals(url)) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
