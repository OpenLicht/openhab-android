# Fork of original openHAB Android App

Original remote: <https://github.com/openhab/openhab-android>

## Local setup

- Connect to the Pi running at `141.76.65.29` with the known credentials and a local port-forward from `8080` to `127.0.0.1:8080`, or use the following ssh config:

```
Host openhab-pi
  HostName 141.76.65.26
  User pi
  LocalForward 8080 127.0.0.1:8080
```

- Now, you can open <http://localhost:8080>
- To use this within the openHAB app, change the settings to not use demo mode, and the local server to `http://10.0.2.2:8080` (See [a StackOverflow thread](https://stackoverflow.com/questions/5806220/how-to-connect-to-my-http-localhost-web-server-from-android-emulator-in-eclips) why to use this IP instead of `localhost`)
